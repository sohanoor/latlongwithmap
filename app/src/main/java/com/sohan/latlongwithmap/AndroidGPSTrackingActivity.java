package com.sohan.latlongwithmap;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AndroidGPSTrackingActivity extends Activity {

    Button btnShowLocation, btnMapLocation;
    TextView myLocation;
    String result, mobileNumber, message;

    // GPSTracker class
    GPSTracker gps;
    //Internet Connection Detector Class
    InternetConnectionDetector internetDetector;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        internetDetector = new InternetConnectionDetector(this);

        btnShowLocation = (Button) findViewById(R.id.btnShowLocation);
        btnMapLocation = (Button) findViewById(R.id.btnMapLocation);
        myLocation = (TextView) findViewById(R.id.mylocation);

        // show location button click event
        btnShowLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // create class object
                gps = new GPSTracker(AndroidGPSTrackingActivity.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

//                    // \n is for new line
                    result = "My Current Location is - \nLatitude: " + latitude + "\nLongitude: " + longitude;
                    //  result = "My Current Location is-Latitude: " + latitude + " Longitude: " + longitude;
                    myLocation.setText(result);
                    //  Toast.makeText(getApplicationContext(), "" + result, Toast.LENGTH_LONG).show();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }
        });

        btnMapLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!internetDetector.isConnectedToInternet()) {
                    showPopUp();
                } else {
                    Intent mapIntent = new Intent(AndroidGPSTrackingActivity.this, MapsActivity.class);
                    startActivity(mapIntent);
                }

            }
        });
    }

    public void showPopUp() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(AndroidGPSTrackingActivity.this);
        alertDialog.setTitle("WARNING !")
                .setMessage("Please check internet connection and try again.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //user pressed OK
                    //    finish();
                    }
                })
                .create()
                .show();
    }
}